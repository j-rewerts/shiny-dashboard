$(document).ready(function () {              
    navigator.geolocation.getCurrentPosition(onSuccess, onError);

    function onError(err) {
      setTimeout(function () {
            Shiny.onInputChange("geolocation", false);
        }, 500)
    }
      
    function onSuccess(position) {
        setTimeout(function () {
            var coords = position.coords;
            Shiny.onInputChange("geolocation", true);
            Shiny.onInputChange("lat", coords.latitude);
            Shiny.onInputChange("lng", coords.longitude);
        }, 500)
    }
})